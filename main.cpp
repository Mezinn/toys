#include <iostream>
#include <map>

#define clear() system("clear");

#include <sqlite3.h>

using namespace std;


class ICommand;

class App;


static int callback(void *data, int argc, char **argv, char **azColName) {
    int i;
    
    for (i = 0; i < argc; i++) {
        printf("%s : %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
    }

    printf("\n");
    return 0;
}


class ICommand {

    friend class App;

protected:
    App *app = nullptr;
public:
    ICommand(App *app) {
        this->app = app;
    }

    virtual string getKey() {
        return " ";
    }

    virtual string getDescription() {
        return " ";
    }

    virtual void execute() {};

};

class App {


    friend class ICommand;

protected:
    map<string, ICommand *> *commands;

public:

    App() {
        this->commands = new map<string, ICommand *>();
        this->rc = sqlite3_open("toys.db", &this->db);
        if (rc) {
            fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
            exit(EXIT_FAILURE);
        } else {
            fprintf(stderr, "Opened database successfully\n");
        }
    }

    ~App() {
        delete (this->commands);
        sqlite3_close(db);
    }

    App *addCommand(ICommand *command) {
        this->commands->insert(pair<string, ICommand *>(command->getKey(), command));
    }


    void showMenu() {
        string key;
        while (true) {
            clear();
            cout << "Оберіть дію:" << endl;
            for (pair<string, ICommand *> command: *this->commands) {
                cout << "[" << command.second->getKey() << "]\t" << command.second->getDescription() << endl;
            }
            cin >> key;
            map<string, ICommand *>::iterator command = this->commands->find(key);
            if (command != this->commands->end()) {
                command->second->execute();
                cin.get();
                cin.get();
            }
        }
    }

    int rc;
    sqlite3 *db;
    char *zErrMsg = 0;
};

class CommandA : public ICommand {
    using ICommand::ICommand;

public:
    string getKey() {
        return "A";
    }

    string getDescription() {
        return "Отримати назву іграшок, ціна яких не перевищує 140 грн. і які підходять дітям 5 років.";
    }

    void execute() {

        char *sql = "select type as 'Тип', name as 'Назва' from toys where price < 140 and limitFrom =5 or limitTo =5;";
        const char *data = "";

        this->app->rc = sqlite3_exec(this->app->db, sql, callback, (void *) "", &this->app->zErrMsg);

        sqlite3_free(this->app->zErrMsg);

    }
};

class CommandB : public ICommand {

    using ICommand::ICommand;

public:
    string getKey() {
        return "B";
    }

    string getDescription() {
        return "Визначити вартість найдорожчого конструктора.";
    }

    void execute() {

        char *sql = "select price as 'Ціна' from toys WHERE toys.type = 'Конструктор' order by toys.price desc limit 1;";
        const char *data = "";

        this->app->rc = sqlite3_exec(this->app->db, sql, callback, (void *) "", &this->app->zErrMsg);

        sqlite3_free(this->app->zErrMsg);

    }
};

class CommandC : public ICommand {

    using ICommand::ICommand;

public:
    string getKey() {
        return "C";
    }

    string getDescription() {
        return "Надрукувати назву найбільш дорогих іграшок(ціна яких відрізняється від ціни самої дорогої іграшки не більше ніж на 50 грн.).";
    }

    void execute() {

        char *sql = "select name as 'Назва', price as 'Ціна' from toys where price >= (select price from toys order by price DESC limit 1) - 50;";
        const char *data = "";

        this->app->rc = sqlite3_exec(this->app->db, sql, callback, (void *) "", &this->app->zErrMsg);

        sqlite3_free(this->app->zErrMsg);


    }
};

class CommandD : public ICommand {

    using ICommand::ICommand;

public:
    string getKey() {
        return "D";
    }

    string getDescription() {
        return "Отримати назви іграшок, які підходять дітям як чотирьох, так і десяти років";
    }

    void execute() {

        char *sql = "SELECT name as 'Назва' FROM toys WHERE limitFrom IN (4,10) or limitTo IN (4,10); ";
        const char *data = "";

        this->app->rc = sqlite3_exec(this->app->db, sql, callback, (void *) "", &this->app->zErrMsg);

        sqlite3_free(this->app->zErrMsg);


    }
};


class CommandE : public ICommand {

    using ICommand::ICommand;

public:
    string getKey() {
        return "E";
    }

    string getDescription() {
        return "Отримати відомості про те, чи можна підібрати іграшку, будь-яку, крім м’яча, відповідну дитині трьох років.";
    }

    void execute() {


        char *sql = "SELECT count(*) as 'У наявності' from toys where not type like \"%м'яч\" and limitFrom >=3;";
        const char *data = "";

        this->app->rc = sqlite3_exec(this->app->db, sql, callback, (void *) "", &this->app->zErrMsg);

        sqlite3_free(this->app->zErrMsg);


    }
};

class CommandF : public ICommand {

    using ICommand::ICommand;

public:
    string getKey() {
        return "F";
    }

    string getDescription() {
        return "Отримати назву найдешевшої іграшки.";
    }

    void execute() {


        char *sql = "select type as 'Тип', name as 'Назва' from toys order by price asc limit 1;";
        const char *data = "";

        this->app->rc = sqlite3_exec(this->app->db, sql, callback, (void *) "", &this->app->zErrMsg);

        sqlite3_free(this->app->zErrMsg);


    }
};

class CommandG : public ICommand {

    using ICommand::ICommand;

public:
    string getKey() {
        return "G";
    }

    string getDescription() {
        return "Отримати назву самої дорогої іграшки для дітей до чотирьох років.";
    }

    void execute() {


        char *sql = "select type as 'Тип', name as 'Назва' from toys WHERE limitFrom = 4 or limitTo = 4 order by price DESC limit 1;";
        const char *data = "";

        this->app->rc = sqlite3_exec(this->app->db, sql, callback, (void *) "", &this->app->zErrMsg);

        sqlite3_free(this->app->zErrMsg);


    }
};

class CommandH : public ICommand {

    using ICommand::ICommand;

public:
    string getKey() {
        return "H";
    }

    string getDescription() {
        return "Отримати назви іграшок для дітей чотирьох-п’яти років.";
    }

    void execute() {


        char *sql = "select type as 'Тип', name as 'Назва' from toys WHERE limitFrom in (4,5) or limitTo IN (4,5);";
        const char *data = "";

        this->app->rc = sqlite3_exec(this->app->db, sql, callback, (void *) "", &this->app->zErrMsg);

        sqlite3_free(this->app->zErrMsg);


    }
};

class CommandI : public ICommand {

    using ICommand::ICommand;

public:
    string getKey() {
        return "I";
    }

    string getDescription() {
        return "Отримати назву самої дорогої іграшки, підходящої дітям двох-трьох років.";
    }

    void execute() {


        char *sql = "select type as 'Тип', name as 'Назва' from toys WHERE limitFrom in (2,3) or limitTo IN (2,3) order by price desc limit 1;";
        const char *data = "";

        this->app->rc = sqlite3_exec(this->app->db, sql, callback, (void *) "", &this->app->zErrMsg);

        sqlite3_free(this->app->zErrMsg);

    }
};

class CommandJ : public ICommand {

    using ICommand::ICommand;

public:
    string getKey() {
        return "J";
    }

    string getDescription() {
        return "Визначити вартість найдорожчої ляльки.";
    }

    void execute() {


        char *sql = "select price as 'Ціна' from toys where type = 'Лялька' order by price desc limit 1;";
        const char *data = "";

        this->app->rc = sqlite3_exec(this->app->db, sql, callback, (void *) "", &this->app->zErrMsg);

        sqlite3_free(this->app->zErrMsg);

    }
};

class CommandK : public ICommand {

    using ICommand::ICommand;

public:
    string getKey() {
        return "K";
    }

    string getDescription() {
        return "Визначити вартість ляльок для дітей шести років.";
    }

    void execute() {


        char *sql = "select SUM(price) as 'Вартість' from toys where type = 'Лялька' group by type;";
        const char *data = "";

        this->app->rc = sqlite3_exec(this->app->db, sql, callback, (void *) "", &this->app->zErrMsg);

        sqlite3_free(this->app->zErrMsg);
    }
};

class CommandL : public ICommand {

    using ICommand::ICommand;

public:
    string getKey() {
        return "L";
    }

    string getDescription() {
        return "Для дітей якого віку призначається конструктор.";
    }

    void execute() {

        char *sql = "select MIN(limitFrom) as 'Від', MAX(limitTo) as 'До' from toys where type = 'Конструктор';";
        const char *data = "";

        this->app->rc = sqlite3_exec(this->app->db, sql, callback, (void *) "", &this->app->zErrMsg);

        sqlite3_free(this->app->zErrMsg);

    }
};

class CommandM : public ICommand {

    using ICommand::ICommand;

public:
    string getKey() {
        return "M";
    }

    string getDescription() {
        return "Для дітей якого віку призначені кубики? Вказати їх середню вартість";
    }

    void execute() {

        char *sql = "select MIN(limitFrom) as 'Від', MAX(limitTo) as 'До', (select AVG(price) as 'в середньому' from toys where type = 'Кубики' group by price) as Avg  from toys where type = 'Кубики';";
        const char *data = "";

        this->app->rc = sqlite3_exec(this->app->db, sql, callback, (void *) "", &this->app->zErrMsg);

        sqlite3_free(this->app->zErrMsg);


    }
};

class ExitCommand : public ICommand {

    using ICommand::ICommand;

public:

    string getKey() {
        return "~";
    }

    string getDescription() {
        return "Вихід з програми.";
    }

    void execute() {
        exit(EXIT_SUCCESS);
    }

};


int main() {


    App *app = new App();
    app->addCommand(new CommandA(app));
    app->addCommand(new CommandB(app));
    app->addCommand(new CommandC(app));
    app->addCommand(new CommandD(app));
    app->addCommand(new CommandE(app));
    app->addCommand(new CommandF(app));
    app->addCommand(new CommandG(app));
    app->addCommand(new CommandH(app));
    app->addCommand(new CommandI(app));
    app->addCommand(new CommandJ(app));
    app->addCommand(new CommandK(app));
    app->addCommand(new CommandL(app));
    app->addCommand(new CommandM(app));
    app->addCommand(new ExitCommand(app));

    app->showMenu();


}